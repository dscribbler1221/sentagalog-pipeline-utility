require 'csv'
require 'json'
require 'yaml'
require 'descriptive_statistics'
require_relative 'tools/helpers/database'
require_relative 'tools/helpers/marker'

# ----------- INPUT HELPERS ------------- #
def inputs(mode=STDIN)
    input = mode.gets 
    if input == nil || input == "" then 
        exit
    end 
    input.chomp
end 

def validate_input(prompt, valid_set) 
    while true 
        print prompt 
        input = inputs
        valid = valid_set.include? input 
        yield input, valid
    end 
end 

def print_if(verbose, str)
    print str
end 

# ----------- SYSTEM COMMAND HELPERS ------------- #
def run_node(path, arguments=[]) 
    `node #{path}.js #{arguments.join(" ")}`
end 

def run_python(path, arguments=[]) 
    `python3 #{path}.py #{argument.join(" ")}`
end 

def run_ruby(path, arguments=[]) 
    `ruby #{path}.rb #{argument.join(" ")}`
end 


# ----------- TEXT FILE HELPERS ------------- #
def read_file(path, mode="r:UTF-8")
    File.open(path, mode, &:read)
end

def write_file(path, data="", mode="w:UTF-8")
    file = File.open(path, mode)
    file << data
    file
end 

# ----------- JSON FILE HELPERS ------------- #
def read_json(path, mode="r:UTF-8")
    file = read_file(path, mode)
    JSON.parse(file)
end 

def write_json(path, data="", prettify=true, mode="w:UTF-8")
    json_str = "" 
    if prettify then 
        json_str = JSON.pretty_generate(data)
    else
        json_str = JSON.generate(data)
    end 
    write_file(path, json_str, mode)
end

# ----------- CONVERTER UTILITY ---------------- #
def json2csv(json_file, csv_file, verbose=false) 
    json_arr = read_json(json_file)
    
    # loop through the JSON array 
    CSV.open(csv_file, "w:UTF-8") do |csv|
        # print headers 
        print_if(verbose, "json2csv . writing headers\n")
        csv << json_arr[0].keys

        # print rows
        i = 0
        json_arr.each do |row|
            print_if(
                verbose, 
                "json2csv . writing row #{i + 1} of #{json_arr.length}\n"
            )
            csv << row.values 
            i += 1
        end 
        
        # print done 
        print_if(verbose, "json2csv . done\n")
    end
end 

def csv2json(csv_file, json_file, prettify=true, verbose=false)
    csv_str = read_file(csv_file)
    csv_data = CSV.parse(csv_str, headers:true)
    json_obj = []

    # print headers
    print_if(verbose, "csv2json . identifying headers\n")
    headers = csv_data.headers

    # loop through the csv data
    i = 0
    csv_data.each do |row| 
        item = {}
        print_if(
            verbose, 
            "csv2json . writing row #{i + 1} of #{csv_data.length}\n"
        )
        headers.each do |header| 
            item[header] = row[header]
        end
        json_obj.push(item)
        i += 1
    end 


    puts "csv2json . writing file\n"
    if prettify then 
        write_json(json_file, json_obj, true)
    else
        write_json(json_file, json_obj)
    end 

    puts "csv2json . done\n"

end 

def rc_table2csv(csv_file, rc_table, verbose=true)
    # get column headers
    first = rc_table[rc_table.keys[0]]
    column_headers = first.keys

    CSV.open(csv_file, "w:UTF-8") do |csv| 
        # print column headers 
        print_if(verbose, "rc_table2csv . printing headers\n")
        csv << [""] + column_headers 

        # print rows
        i = 0 
        rc_table.each do |row_header, row_data| 
            print_if(
                verbose, 
                "rc_table2csv . writing row #{i + 1} of #{rc_table.length}\n"
            )
            csv << [row_header] + row_data.values
            i += 1
        end 
    end


    puts "rc_table2csv . done\n"
end 


def xy_table2csv(csv_file, xy_table, headers=["field", "value"], verbose=true) 
    CSV.open(csv_file, "w:UTF-8") do |csv| 
        # print column headers 
        print_if(verbose, "xy_table2csv . printing headers\n")
        csv << headers

        # print rows 
        i = 0
        xy_table.each do |key, val| 
            print_if(
                verbose, 
                "xy_table2csv. writing row #{i + 1} of #{xy_table.length}\n"
            )
            csv << [key, val]
            i += 1
        end 
    end

    puts "xy_table2json . done\n"
end


# ----------- DISPLAY UTILITY -------------- #
def display_divider(sep="=", len=50) 
    sep * len 
end 

def clear_screen 
    system("clear")
end 


def set_interval(delay)
    Thread.new do
      loop do
        sleep delay
        yield 
      end
    end
end


def set_interval_sync(delay)
    loop do
        sleep delay
        yield 
    end
end


def clear_interval(interval)
    interval.kill
end

# ------------- YAML ---------------- #
def load_yaml(file) 
    YAML.load_file(file)
end


# ---------- REPORT FILES ----------- #
def create_report(file)
    Marker.new file
end


def ls_files(folder)
    Dir.entries(folder).select {|f| !File.directory? f}
end

# -------- math functions ---------------_#
def get_sum_of(field, context)
    context.values.collect { |v| v[field] }.sum
end

def get_average_of(field, context) 
    sum = get_sum_of(field, context)
    sum / context.length
end

def get_max_of(field, context)
    context.values.collect { |v| v[field] }.max
end

def get_min_of(field, context)
    context.values.collect { |v| v[field] }.min
end

def get_range_of(field, context)
    max = get_max_of(field, context)
    min = get_min_of(field, context)
    max - min 
end

def get_values(field, context)
    context.values.collect { |v| v[field] }
end

def get_stdev_of(field, context)
    get_values(field, context).standard_deviation.round(3)
end

def get_variance_of(field, context) 
    get_values(field, context).variance.round(3)
end

def get_stat_info(field, context)
    {
        "no. of items" => context.length,
        "total" => get_sum_of(field, context),
        "average"=> get_average_of(field, context),
        "max" => get_max_of(field, context), 
        "min" => get_min_of(field, context),
        "range" => get_range_of(field, context), 
        "stdev" => get_stdev_of(field, context), 
        "variance" =>  get_variance_of(field, context),
        "cv" => get_stdev_of(field, context) / get_average_of(field, context)
    }
end

def derive_vars(field, meta, context, round)
    context.each do |key, values|
        value = values[field] 

        context[key]["percentage (" + field + ")"] = 
            (value * 1.0 / meta["average"]).round(round)

        context[key]["z-score (" + field + ")"] = 
            ((value * 1.0 - meta["average"]) / meta["stdev"]).round(round)


        context[key]["mean-normalized (" + field + ")"] = 
            ((value * 1.0 - meta["average"]) / meta["range"]).round(round)
    end
end

#
#   myobj[.myvar.myvalr]   <-- [access string]   
#
def access_string_get(object, access_string)
    if access_string.class != String then 
        return access_string
    end

    if access_string[0] == "$" then 
        commands = access_string.split("$") 
        res = nil
        commands.each do |script| 
            res = eval(script)            
        end
        return res
    end

    # split access string by periods 
    access_tokens = access_string.split(".")
    access_base = object
    
    # access the first item 
    while access_tokens.shift 
        token = access_tokens[0]

        if access_base[token].class == Hash then
            access_base = access_base[token]
        else
            return access_base[token]
        end
    end
end

def idx(headers, col_name)
    headers.index(col_name)
end