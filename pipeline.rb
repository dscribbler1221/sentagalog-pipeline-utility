#!/usr/bin/env ruby
require_relative "preload"
require_relative "tools/project"
require_relative "tools/helpers/slim_input"
require "io/console"
config = load_yaml("config.yaml")

run_name = "unnamed run"
run_desc = "just a test run"
script = nil

if ARGV.include? "--name" then
    at = ARGV.index("--name")
    run_name = ARGV[at + 1]
    run_desc = ARGV[at + 2] 
end

if ARGV.include? "--script" then 
    at = ARGV.index("--script")
    script = ARGV[at + 1]
end

$script = script

class Pipeline 
    @runtime_name
    @slim_input
    @project
    @vars


    def initialize(run_name="unnamed run", run_desc="just a test run", config)
        @report = create_report("reports/pipeline.md") 
        @runtime_name = {
            "run_name" => run_name, 
            "run_desc" => run_desc,
            "run_at"   => Time.now.to_s,
            "run_id"   => Time.now.to_i.to_s
        }
        @vars = {}


        @project = Project.new config, @runtime_name
        @slim_input = SlimInput.new @project, @vars, self

    
        @vars["SLIM_INPUT_CONTENT"] = read_file("slim_input_screen")
        
        @report.write "# Pipeline Runtime Report"
        @project.report_runtime_info(@report)
        @project.report_global_config(@report)
    
        clear()

        puts "---DONE---"
        gets
    end


    # ---------------------------- MAIN FUNCTION ------------------------- #
    def main() 
        puts "PROJECT SENTAGALOG PIPELINE UTILITY"
        puts
        puts display_divider(sep="=", 70)
        puts "v.1.0 - Kevin Baldo, Ian Manaog, LJ Sta. Ana"
        puts "Please refer to `docs/manual.md` for manual"
        puts "> Markdown reports for module configuration are at `reports/`"
        puts "> Please use this program only to execute methods."
        puts "> Type 'slim_input' for a minimal 2 char code -> command version."
        puts display_divider(sep="=", 70)
        puts "Run Name: #{@runtime_name["run_name"]}"
        puts "Run Description: #{@runtime_name["run_desc"]}"
        puts "Please enter command..."
        puts

        if $script != nil && $script != "" then 
            clear_screen
            exec_script($script) 
            exit
        end

        loop do
            # SENTAGALOG PIPELINE UTILITY REPL
            print "> " 
            begin
                eval(inputs)
                
            rescue => exception 
                puts exception.backtrace
                puts exception.inspect
                puts "Invalid command."
            end
        end
    end

    # ------- shorthand functions ----- #


    def edit_search_terms 
        system("nano #{@project.get_config("collector")["search_terms"]}")
    end 

    def view_search_terms 
        system("cat #{@project.get_config("collector")["search_terms"]}")
    end 
    
    def exec_script(script) 
        eval(`cat #{script}`)
    end

    def collection_partition(partition) 
        @project.collector.collect_partition(partition)
    end

    def collect_all(start=0)
        @project.collector.start_collection_sequence(start)
    end

    def aggregate(source="dropzone")
        @project.dataset_builder.aggregate(source)
    end

    def view_aggregation_info(source="dropzone")
        @project.dataset_builder.aggregation_info(source)
    end

    def view_build_info(dataset)
        @project.dataset_builder.build_info(dataset)
    end

    def build_dataset(dataset)
        @project.dataset_builder.build(dataset)
    end

    def clear_database 
        proceed_input {
            @project.database.clear_database
        }
    end

    def clear_database_table(table_name)
        proceed_input {
            @project.database.clear_database_table table_name
        }
    end


    def archive_dropzone(name)
        current_folder = @project.get_config("collector")["driver"]["dropzone"]
        new_folder = @project.get_config("collector")["driver"]["archives"] + name
        puts "Archiving dropzone."
        puts "Making directory"
        system("mkdir ./#{new_folder}")
        puts "Backing up dropzone to archives folder."
        puts "Recreating dropzone."
        system("sudo cp -r #{current_folder}* #{new_folder}")
        puts "Successful"
    end
    
    def reload_archive(name)
        puts "Warning: This may cause data loss"
        puts "If you haven't yet, please back up the current dropzone first."
        puts "Are you sure? Type `proceed` to continue."
        validate_input("> ", ["proceed"]) {
            |input, valid| 
            if valid then 
                reload_dropzone_do(name)
                break
            else 
                break
                return
            end
        }
    end

    def reload_dropzone_do(name)
        puts "Reloading dropzone `#{name}`"
        puts "Moving dropzone..."
        dropzone_folder = @project.get_config("collector")["driver"]["dropzone"]
        old_folder = @project.get_config("collector")["driver"]["archives"] + name
        system("rm -rf #{dropzone_folder}*")
        system("cp -r #{old_folder}/meta #{dropzone_folder}")
        system("cp -r #{old_folder}/tweets #{dropzone_folder}")
        puts "Successful."
    end

    def clear_dropzone
        puts "Are you sure you want to clear the dropzone folder?"
        puts "Warning: This may cause data loss. "
        puts "Please backup first if you haven't yet."
        puts "Type `proceed` to continue."
        validate_input("> ", ["proceed"]) {
            |input, valid| 
            if valid then 
                clear_dropzone_do
                break
            else 
                break
                return
            end
        }
    end

    def clear_dropzone_do 
        dropzone_folder = @project.get_config("collector")["driver"]["dropzone"]
        puts "Clearing dropzone."
        system("rm -rf #{dropzone_folder}/*")
        system("mkdir ./#{dropzone_folder}meta")
        puts "Rebuilding dropzone subfolder s."
        system("mkdir ./#{dropzone_folder}tweets")
    end

    def view_dropzone
        puts `ls #{@project.get_config("collector")["driver"]["dropzone"]}tweets`
    end


    # ------------- slim version --------------- #

    def slim_input 
        clear_screen()
        print @vars["SLIM_INPUT_CONTENT"] + " "

        code = inputs
        execute_command(code)
        
        puts "Please press enter to continue."
        inputs
        slim_input 
    end 

    def execute_command(code)
        # General Operations
        if code == "a1" then 
            @slim_input.execute_script
        elsif code == "a2" then 
            @slim_input.view_search_terms 
        elsif code == "a3" then 
            @slim_input.edit_search_terms 

        # Project Operations
        elsif code == "b1" then 
            @slim_input.view_config
        elsif code == "b2"  then 
            @slim_input.view_task_config
        elsif code == "b3" then
            @slim_input.clear_database_table 
        elsif code == "b4" then 
            @slim_input.clear_database 
        
        # Collection Operations
        elsif code == "c1" then 
            @slim_input.collect_partition
        elsif code == "c2" then 
            @slim_input.collect_all 
        elsif code == "c3" then 
            @slim_input.update_report_meta
        elsif code == "c4" then
            @slim_input.archive_dropzone 
        elsif code == "c5" then
            @slim_input.reload_archive 
        elsif code == "c6" then
            @slim_input.clear_dropzone 
        elsif code == "c7" then
            @slim_input.view_dropzone

        # Dataset Building Operations
        elsif code == "d1" then 
            @slim_input.view_aggregation_info("dropzone")
        elsif code == "d2" then
            @slim_input.view_aggregation_info("imports")
        elsif code == "d3" then 
            @slim_input.aggregate("dropzone")
        elsif code == "d4" then
            @slim_input.aggregate("imports")
        elsif code == "d5" then
            @slim_input.view_build_info
        elsif
            @slim_input.build_dataset
       

        # Data Preprocessing 
        elsif code == "e1" then
            @slim_input.perform_std_cleaning
        elsif code == "e2" then 
            @slim_input.perform_eiou_respelling

        # Feature Building 
        elsif code == "f1" then 
            @slim_input.perform_smiley_tfidf 
        elsif code == "f2" then 
            @slim_input.perform_stream_of_words
        elsif code == "f3" then 
            @slim_input.perform_bag_of_words
        
        # Feature Selection 
        elsif code == "g1" then 
            @slim_input.build_feature_space
        elsif 
            @slim_input.reduce_features 
        else 
            puts "Invalid command."
        end
    end


    ###############################################

    def proceed_input
        puts "Proceed? Are you sure this may cause data loss."
        puts "Please type `proceed` to continue."
        validate_input("> ", ["proceed"]){
            |input, valid| 
            
            if valid then
                yield
            else 
                puts "Invalid command. Exiting."
                break
            end
        }
    end


    ###############################################

    # ------ restarts the program ------- #
    def restart
        system("clear")
        exec("ruby pipeline.rb")
    end

    # clears the program
    def clear
        system("clear")
        main()
    end
end

pipeline = 
    Pipeline.new(run_name, run_desc, config)