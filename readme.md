# PROJECT SENTAGALOG PIPELINE UTILITY
This project implements a pipeline utility for Project Sentagalog. It implements a: 

1. data collector
2. dataset builder
3. data preprocessor 
4. feature builder 
5. feature space selector


## Project Sentagalog
Project Sentagalog a research project that experiments on three proposed approaches on sentiment analysis of Filipino-English tweets. 

1. **E-I/O-U Respelling**
	* E-I/O-U Respelling handles cases in Tagalog language where words retain the same meaning even if some vowels are changed. For example, the word `lalake` - `lalaki` and `grabe` and `grabi`. In this procedure, all vowels `e` in  are converted to `i` and vowels `o` are converted to `u` in Filipino words. Non-Tagalog words are not affected.
2. **SmileyTFIDF**
	* SmileyTFIDF makes use of TFIDF to associate words to different emojis. SmileyTFIDF is a statistical measure of a term's exclusivity to a certain topic versus other topics. 
	* For example, if the word `dog` is to be tested against topics such as `mammals`, `reptiles` and `amphibians`. It is most likely to have a good association with the topic `mammals` versus other topics since the other topics is predictably not involving the topic dog except for some cases such predation.
	* On the contrary, if the word `dog` is to be tested against topics where it may appear uniformly such as breeds of dogs i.e `pitbulls`, `chihuahua` or `dalmatians`, the word dog may have less of exclusivity to each topic since all are about dogs.
	* TF-IDF is computed by counting the frequency of a word or term in a certain topic over a set of data containing different topics and then reducing that count by the number of other documents that also contain the word which lessens the exclusivity of the word to the topic. The formula for TF-IDF is the following. 
	
			term = "dog"
			documents = {
			"mammals" : [...set of sentences about mammal...],
			"reptiles" : [...set of sentences about reptiles...],
			"amphibians" : [..set of sentences about amphibians...]
			}

			no_of_documents_that_have_term = count(term, documents)

			term_frequency(term, document) = frequency(term, document)
			inverse_document_frequency(term) = log(no_of_documents/no_of_documents_that_have_term)

			tf_idf(term, document) = term_frequency(term, document) * inverse_document_frequency(term) 

  
  * **SmileyTFIDF**
      *  SmileyTFIDF a modification of such measure such that $T$ (set of terms) is the set of all vocabulary words in the corpus while $D$ is a set of predefined smileys set as documents that each term will be associated with. 
         
            term = "happy"
            documents = {
              "😀" : [...set of tweets that contain the emoji 😀...],
              "😇" : [...set of tweets that contain the emoji 😇...],
              "🤬" : [...set of tweets that contain the emoji 🤬...]
              ...
            }

            no_of_documents_that_have_term = count(term, documents)

            term_frequency(term, document) = frequency(term, document)
            inverse_document_frequency(term) = log(no_of_documents/no_of_documents_that_have_term)

            smiley_tf_idf(term, document) = term_frequency(term, document) * inverse_document_frequency(term) 

            feature_space(tweet) = [...sum of smiley_tf_idfs of each term/word in the tweet for each smiley...]
           ```
         

3. **Stream-of-Words**
	* Stream-of-Words is a feature space approach for describing the series of words that appear in a short text such as a tweet. It makes use of TFIDF to measure the polarity of words over the sentiments `positive`, `negative` and `neutral` and creates a feature subspace for each where `positive_weights`, `negative_weights` and `neutral_weights` are the sets of all TF-IDF values in the text associated with the label `positive`, `neutral` and `negative` respectively.
   
     * **Sentiment TF-IDF + Stream-of-Words**
              
            term = "happy"
            documents = {
              "+" : [...set of tweets that contain positive label...],
              "-" : [...set of tweets that contain negative label...],
              "~" : [...set of tweets that contain neutral label...]
            }

            no_of_documents_that_have_term = count(term, documents)

            term_frequency(term, document) = frequency(term, document)
            inverse_document_frequency(term) = log(no_of_documents/no_of_documents_that_have_term)

            tf-idf(term, document) = term_frequency(term, document) * inverse_document_frequency(term)

            feature_space(positive, tweet) = [...array of sentiment_tfidf of each term/word in the tweet...]

         


**Normalization and Weighting Schemes**

This program also implements different third-party weighting schemes for TFIDF such as `DeltaTFIDF` and `BM25` as modified versions of the standard TFIDF used for building the feature resources. More details about the approaches and other theoretical background and specific methodologies are found in `docs/whitepaper.md`. 

## Requirements
The following are the requirements for running the program. 

1. **Ruby** - used for the command line pipeline utility
	* **CSV Gem**
	* `gem install csv`
	* **JSON Gem**
	* `gem install json`
	* **SQLite3 Gem**
	* `gem install sqlite3`
	* **Descriptive Statistics Gem**
	* `gem install descriptive_statistics`
2. **Node.js** - used for implementing a collector client for TwitterAPI and <del>a simple web application for interactive visualizations of the dataset</del> (under construction). 
> The following node_modules are automatically installed by `package.json` and are bundled with the repository files. Only execute the commands below if you want to build by yourself or if something's wrong.
	* **Twit**
	* `npm install --save twit`
	* **Express**
	* `npm install --save express`
	* **SQLite3**
	* `npm install sqlite3`
	* **Knex** 
	* `npm install knex`


## Installation

**Getting the Project Files**
1. Download an archive of this repo 
2. Or, clone this repo into your local system:

	$ git clone http://bitbucket.org/dscribbler1221/sentagalog-pipeline-utility.git/

**Test Running the Pipeline Utility**

Try to run the pipeline utility after grabbing the files by running the following command.

	$ cd path/to/project/ 
	$ ruby pipeline.sh

You can also make an executable version of the pipeline utility.

	$ sudo chmod +x pipeline.rb

To run the executable version.

	$ ./pipeline.rb
	

#### Raw Eval Mode
You can now create a shortchut or a symlink for the pipeline utility to make it accessible. When you do, you just have to click the program and a command line GUI will appear.

```text
PROJECT SENTAGALOG PIPELINE UTILITY
======================================================================
v.1.0 - Kevin Baldo, Ian Manaog, LJ Sta. Ana
Please refer to `docs/manual.md` for manual
> Markdown reports for module configuration are at `reports/`
> Please use this program only to execute methods.
> Type 'slim_input' for a minimal 2 char code -> command version.
======================================================================
Run Name: unnamed run
Run Description: just a test run
Please enter command...
> 
```


This is the active `eval(command)` or REPL-like version of the program intended for flexible and programmable use cases. To learn the commands that can be executed by this mode, please refer to the `docs/manual.md` file that gives a detailed breakdown of several of the possible commands that can be inputted.

#### Slim-UI Mode
To enter a simpler version for accessibility type `slim_input`. The slim input is `2 char code -> command` version that enables guided prompts without having to know the underlying structure of the project. 

	Please enter command...
	> slim_input


This command will provide a simpler way of accessing the pipeline utility features by focusing on the core commands that are routinary in the course of the experiment. These may not offer as much flexibiliy as what is provided raw eval mode. However, most of the actions and tasks involved are readily handled by this mode. It may also give the user an overview of the general flow of the activities involved in the project/experiment.
	
``` text
SENTAGALOG PIPELINE UTILITY
================================================
Slim-Input version

Please enter a command from the following tasks.
Make sure to configure pipeline using config.yaml 
first.

General Operations 
a1 - execute script
a2 - view search terms
a3 - edit search terms 

Project Operations
b1 - view config
b2 - view component config
b3 - clear database table 
b4 - clear all tables in database

Collection Operations 
c1 - collect partition
c2 - collect all (starting from index)
c3 - update report meta
c4 - archive dropzone
c5 - reload archive
c6 - clear dropzone
c7 - view dropzone

Dataset Building Operations 
d1 - view aggregation info (dropzone)
d2 - view aggregation info (imports)
d3 - aggregate dropzone items
d4 - aggregate import items
d5 - view build info
d6 - build raw/unclean dataset

Data Preprocessing 
e1 - perform standard cleaning on raw/unclean dataset 
e2 - perform eiou respelling on std. cleaned dataset

Feature Building
f1 - perform smiley-tdfidf
f2 - perform stream-of-words
f2 - perform bag-of-words

Feature Space Selection
g1 - build feature space 
g2 - reduce features

Command :
```