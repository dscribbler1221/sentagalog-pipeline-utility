// load dependencies
let Twit = require('twit')
let fs = require('fs')
let yaml = require('js-yaml')

let config_path = "config.yaml"
let config_str =  fs.readFileSync(config_path, { encoding: "utf8"})
let config = yaml.load(config_str)

// load payload
let payload_path = config["collector"]["driver"]["payload_file"]
let payload_str = fs.readFileSync(payload_path, { encoding: "utf8"})
let payload = JSON.parse(payload_str)

// create collector
let collector_config = payload["collector_options"]
let tokens = payload["tokens"]
for(let token_name in tokens) {
    let token_value = tokens[token_name]
    collector_config[token_name] = token_value
}

let twit = new Twit(collector_config)

// get search items 
let search_items = payload["search_items"]

// search all items in search items
search_items.forEach(item => {
    let search_options = payload["search_options"]
    console.log("Searching for " + item.join(" "))
    search_options.q = item.join(" ")

    if(item[0] != "") {
        let request_time = (new Date()).getTime()
        twit.get("search/tweets", search_options, (err, data, response) => {
            let query_item = item.join(" ")

            // compute meta
            let search_term = item[0]
            let search_filter = item[1]

            let meta = {
                request_time,
                response_time : (new Date()).getTime(),
                count: data.statuses.length,
                next_results: data.search_metadata.next_results,
                search_term,
                search_filter 
            }

            // save meta file
            meta_str = JSON.stringify(meta)
            meta_folder = config["collector"]["driver"]["dropzone"] + "meta"
            meta_filename = query_item + ".meta.json"
            meta_file = meta_folder + "/" + meta_filename
            console.log("Writing " + meta_file + ".")
            fs.writeFileSync(meta_file, meta_str, (err) => {
                console.log("There was an error in writing meta file " + meta_file + ".")
            })

            // save tweet file
            tweet_str = JSON.stringify(data)
            tweet_folder =  config["collector"]["driver"]["dropzone"] + "tweets"
            tweet_filename = query_item + ".json"
            tweet_file = tweet_folder + "/" + tweet_filename
            console.log("Writing " + tweet_file + ".")
            fs.writeFileSync(tweet_file, tweet_str, (err) => {
                console.log("There was an error in writing tweet file " + tweet_str + ".")
            })
        })
    }
})

