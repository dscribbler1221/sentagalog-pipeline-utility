// load dependencies
let Twit = require('twit')
let fs = require('fs')

// load payload
let payload_path = "tools/drivers/collector/in/payload.json"
let payload_str = fs.readFileSync(payload_path, { encoding: "utf8"})
let payload = JSON.parse(payload_str)

// create collector
let collector_config = payload["collector_options"]
let tokens = payload["tokens"]
for(let token_name in tokens) {
    let token_value = tokens[token_name]
    collector_config[token_name] = token_value
}

let twit = new Twit(collector_config)


// search all items in search items
twit.get("application/rate_limit_status", (err, data, response) => {
    limits_file = "tools/drivers/collector/out/limits.json"
    fs.writeFileSync(limits_file, JSON.stringify(data))
})