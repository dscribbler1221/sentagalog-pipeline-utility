require_relative "../../preload"

class SlimInput
    @project
    @vars 
    @pipeline

    def initialize(project, vars, pipeline)
        @project = project 
        @vars = vars 
        @pipeline = pipeline
    end

    # General Operations 
    def execute_script 
        clear_screen 
        puts display_divider
        puts "Which script do you want to execute?"
        puts display_divider
        puts "Please enter path script name:" 
        script = inputs
        script = "scripts/" + script + ".rb"
        puts
        puts "Viewing #{script}..."
        puts display_divider "-"
        system("cat #{script}") 
        puts
        puts display_divider "-"
        puts
        puts "Is the script above valid?"
        @pipeline.proceed_input {
            "Executing script..."
            puts
            puts "--- OUTPUT ---"
            @pipeline.exec_script(script)
            puts "--------------"
            return
        }
    end

    def view_search_terms 
        clear_screen 
        puts display_divider
        puts "Search Terms"
        puts display_divider
        @pipeline.view_search_terms
        puts
    end

    def edit_search_terms 
        clear_screen 
        puts display_divider
        puts "Editing search terms..."
        puts display_divider
        puts "You will now use the program `nano` to edit the search terms."
        @pipeline.proceed_input {
            @pipeline.edit_search_terms
        }
    end

    # Project Operations
    def view_config 
        clear_screen
        puts display_divider
        puts "Global Configuration"
        puts display_divider
        puts JSON.pretty_generate @project.global_config
    end

    def view_task_config 
        clear_screen
        puts display_divider
        puts "Component Configuration"
        puts display_divider
        puts "Which component do you want to display?"
        task = inputs
        puts "------"
        puts JSON.pretty_generate @project.get_config(task)
        puts "------"
    end

    def clear_database_table
        clear_screen 
        puts display_divider 
        puts "Which database table do you want to clear?"
        puts display_divider
        
        validate_input("Please enter table name: ", 
                  @project.get_config("database")["tables"]) {
            |table, valid| 
            if valid then
                @pipeline.proceed_input {
                    @project.database.clear_database_table table
                    return
                }
            else 
                puts "That table does not exist."
                return
            end 
        }

    end

    def clear_database
        clear_screen 
        puts display_divider
        puts "Clearing Entire Database" 
        puts display_divider
        @pipeline.proceed_input {
            @project.database.clear_database
            break
        }
    end

    # Collection Operations
    def collect_partition 
        clear_screen 
        puts display_divider
        puts "Collect Partition" 
        puts display_divider
        puts "Which partition do you want to collect?"
        partition = inputs 
        partition = partition.to_i
        puts "Collecting partition #{partition}"
        @pipeline.proceed_input {
            @project.collector.collect_partition(partition)
            puts "Successfully collected partition #{partition}"
            break
        }
    end

    def collect_all 
        clear_screen 
        puts display_divider
        puts "Collecting All" 
        puts display_divider
        puts "From which partition do you want to start the collection sequence."
        puts "Enter 0 if from the beginning."
        partition = inputs
        partition = partition.to_i
        puts "Starting collection from partition #{partition}"
        @pipeline.proceed_input {
            @project.collector.start_collection_sequence(partition)
            break
        }
    end

    def update_report_meta 
        clear_screen 
        puts display_divider
        puts "Updating Report Meta" 
        puts display_divider
        @project.collector.report_meta 
        puts "Report meta is now updated."
    end

    def archive_dropzone
        clear_screen 
        puts display_divider
        puts "Archive dropzone." 
        puts display_divider
        puts "This operation is safe."
        puts "It only copies files to a backup folder."
        print "Please name this archive: "
        name = inputs 
        @pipeline.archive_dropzone name
    end

    def reload_archive 
        clear_screen 
        puts display_divider
        puts "Reload Archive" 
        puts display_divider
        print "Please archive name: "
        name = inputs
        @pipeline.reload_archive(name)
    end

    def clear_dropzone
        clear_screen 
        puts display_divider
        puts "Clear Dropzone" 
        puts display_divider
        @pipeline.clear_dropzone
    end 

    def view_dropzone 
        clear_screen 
        puts display_divider 
        puts "Dropzone"
        puts display_divider 
        @pipeline.view_dropzone
    end 

    # Dataset Building Operations
    def view_aggregation_info(source)
        clear_screen 
        puts display_divider
        puts "Aggregation Info" 
        puts display_divider
        @project.dataset_builder.aggregation_info(source)
        config = @project.get_config("project")
        markdown_viewer = config["markdown_viewer"]
        system(`#{markdown_viewer} reports/dataset_builder.aggregation-info.md`)
    end 

    def aggregate(source)
        clear_screen 
        puts display_divider
        puts "#{source.capitalize} Aggregator" 
        puts display_divider
        @pipeline.aggregate(source)
    end
    
    def view_build_info
        clear_screen 
        puts display_divider
        puts "Build Info" 
        puts display_divider
        datasets = @project.get_config("dataset_builder")["builder"]
        validate_input("Which dataset? ", datasets) { 
            |dataset, valid|
            if valid then
                @project.dataset_builder.build_info(dataset)
                config = @project.get_config("project")
                markdown_viewer = config["markdown_viewer"]
                system(`#{markdown_viewer} reports/dataset_builder.build-info.md`)
                break
            else
                "Dataset build rules not found."
            end 
        }
    end

    def build_dataset
        clear_screen 
        puts display_divider
        puts "Build Dataset" 
        puts display_divider
        dataset = input 
        @project.dataset_builder.build(dataset)
    end

    # Data Preprocessing
    def perform_std_cleaning 

    end

    def perform_eiou_respelling

    end

    # Feature Building
    def perform_smiley_tfidf

    end

    def perform_stream_of_words

    end

    def perform_bag_of_words

    end

    # Feature Selection
    def build_feature_space

    end 

    def reduce_features

    end
end