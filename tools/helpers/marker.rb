require_relative '../../preload'

class Marker
    @output_file 
    @file

    def initialize(file)
        @output_file = write_file(file)    
        @file = file
    end

    def write(text="", newline="\n") 
        @output_file.syswrite(text + newline)
    end 

    def kv_table(hash_vals, headers=["Field", "Value"])
        write "|#{headers[0]}|#{headers[1]}|", ""
        write
       
        write "|---|---|\n", ""
        hash_vals.each do |key, val| 
            write "|#{key}|#{val}|"
        end 
        write 
    end

    def table(rc_vals, headers=[])
        first = rc_vals[0]
        col_headers = first.keys
        write "|" + col_headers.join("|") + "|"
        write ("|---" * col_headers.length) + "|"
        rc_vals.each do |row| 
            write "|" + row.values.join("|") + "|"
        end
        write
    end

    def rc_table(rc_vals)
        first = rc_vals[rc_vals.keys[0]]
        col_headers = ["table"] + first.keys
        write "|" +  col_headers.join("|") + "|"
        write ("|---" * col_headers.length) + "|"
        rc_vals.each do |row_header, row| 
            write "|" + row_header.to_s + "|" + row.values.join("|") + "|"
        end
        write
    end 


    def rc_table_hooked(rc_vals)
        first = rc_vals[rc_vals.keys[0]]
        col_headers = first.keys
        col_headers[0] = ""
        write "|" + col_headers.join("|") + "|"
        write ("|---" * col_headers.length) + "|"
        rc_vals.each do |row_header, row| 
            yield row_header, row
        end
        write
    end 

    def code(code="", type="json") 
        write "```#{type}"
        write code
        write "```"
    end

    def number_list(array, tab="")
        array.each do |item|
            if item.class != Array then
                write "#{tab}1. #{item}" 
            else 
                number_list(item, tab += "\t")
            end
        end
        write
    end

    def bullet_list(array, tab="")
        array.each do |item|
            if item.class != Array then
                write "#{tab}1. #{item}" 
            else 
                number_list(item, tab += "\t")
            end
        end
        write
    end

    def tabulate(array, tab=20)
      
        # write headers
        write "|", ""
        (0..tab-1).each do |k|
            write "|", ""
        end
        write 
        write "|", ""
        (0..tab-1).each do |k|
            write "---|", ""
        end
        write 

        i = 0
        write "|"
        array.each do |item|
            write(item.to_s + "|", "")
            if i == tab then 
                write 
                i = 0
            end
            i += 1
        end
        write "\n"
    end

    def flash
        close
        reopen
    end

    def reopen 
        @output_file = write_file(@file, "a:UTF-8")    
    end

    def close
        @output_file.close
    end

    def clear
        @output_file = write_file(@file)
    end

    def watch(rate=1)
        set_interval(rate) {
            clear
            yield
        }
    end
end 