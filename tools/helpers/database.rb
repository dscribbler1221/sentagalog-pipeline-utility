require 'sqlite3'

class SentagalogDatabase
    @database
    @config
    @report
    @project
    @statement

    def initialize(config, project)
        @report = create_report("reports/database.md")
        @config = config
        @project = project

        @report.write "# Database Set-up Report \n"
        @project.report_runtime_info(@report)
        @project.report_config("database", @report)
        
        connect()
    end

    def query(sql, bindings=[], db=@database) 
        @statement = db.prepare sql
        i = 1
        bindings.each do |bind|
            @statement.bind_param i, bind
            i += 1
        end
        
        result_set = @statement.execute
    
        while row = result_set.next
            yield row
        end
    end

    def clear_database
        @config["tables"].each do |table_alias, table_name|
            query("DELETE FROM #{table_name}"){}
        end
        puts "Successfully cleared database."
    end

    def clear_database_table table_alias
        query("DELETE FROM #{@config["tables"][table_alias]}") {}
        puts "Successfully cleared database table #{table_alias}."
    end

    def table(name)
        @config["tables"][name]
    end


    def get_all_tweets()
        tweets = []
        query("SELECT * FROM #{table("tweet_repo")}") do |tweet|
            tweets.push(tweet)
        end
        tweets
    end

    def add_tweet(bindings, db=@database)
        sql = """
            INSERT INTO #{table("tweet_repo")} 
            (tweet_id, author, full_text, language, sentiment, timestamp,
            added_at, search_term, search_filter, emojis) 
            VALUES (?,?,?,?,?,?,?,?,?,?)
        """
        query(sql, bindings, db) {}
    end


    def add_meta(bindings, db=@database)
        sql = """
            INSERT INTO #{table("tweet_meta")} 
            (search_term, search_filter, count, collected_at,
             next_results, request_time, response_time) 
            VALUES (?,?,?,?,?,?,?)
        """
        query(sql, bindings, db) {}
    end

    def clear_tweets
        query("DELETE FROM tweet_repo")
        puts "Successfully cleared tweet_repo table."
    end 

    def clear_meta
        query("DELETE FROM tweet_meta")
        puts "Successfully cleared tweet_meta table."
    end

    def clear_all
        clear_tweets
        clear_meta
    end


    def get_var(varname)
        query("SELECT * FROM #{table("variables")} WHERE varname = ?", 
              [varname]) do |variable|
            return variable[1]
        end
    end

    def set_var(varname, value)
        query("""UPDATE #{table("variables")} 
                 SET value = ? 
                 WHERE varname = ?""", 
             [value, varname]) {}
    end

    def init_var(varname, val="undefined", db=@database)
        query("""INSERT INTO #{table("variables")} 
                 (varname, val) 
                 VALUES(?,?)""",
             [varname, value], db) {}
    end

    def delete_var(varname)
        query("""DELETE FROM #{table("variables")}
                WHERE varname = ? """,
             [varname]) {}
    end

    def disconnect
        @database.close
    end

    def connect
        @database = SQLite3::Database.new @config["file"]
    end

    def inspect 
        "Database(@database=#{@database})"
    end

    def transaction()
        @database.transaction do |db| 
            yield db
        end
    end 
end 