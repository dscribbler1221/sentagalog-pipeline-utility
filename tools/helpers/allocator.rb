require_relative "../../preload"

class Allocator
    
    @config 

    def initialize(config)
        @config = config 

        @report = create_report("reports/allocator.md")
        @config = config
    end

    def configuration
        {
            "Terms"                 =>  terms[0..10].join(",") + ",...", 
            "Filters"               =>  filters[0..20].join(",") + ",...", 
            "No. of Terms"          =>  terms_size, 
            "No. of Filters"        =>  filters_size,
            "No. of Search Items"   =>  line_size,
            "Window Size"           =>  window_size, 
            "Window Time"           =>  window_time, 
            "Partition Size"        =>  partition_size, 
            "Parition Time"         =>  partition_time
        }
    end 
    
    def collection_time_information
        collection_time_hrs = 
            "#{total_collection_time/60} hrs and #{total_collection_time%60} mins"
        completion_time_hrs = 
            "#{total_completion_time/60} hrs and #{total_completion_time%60} mins"

        {
            "Partitions to Complete"        => partitions_to_complete, 
            "Max. Partitions per Window"    => partitions_per_window, 
            "Minutes per Partition"         => partition_time, 
            "Min. Window to Complete"       => windows_to_complete, 
            "Minutes per Window"            => window_time, 
            "Total Collection Time (mins)"  => total_collection_time,
            "Total Collection Time (hours)" => collection_time_hrs,
            "Total Completion Time (mins)"  => total_completion_time,
            "Total Completion Time (hours)" => completion_time_hrs
        }
    end

    def collection_amount_information
        collection_time_hrs = 
            "#{total_collection_time/60} hrs and #{total_collection_time%60} mins"
        completion_time_hrs = 
            "#{total_completion_time/60} hrs and #{total_completion_time%60} mins"

        {
            "Max. Tweets per Request"       => max_tweet_request, 
            "No. of Requests per Minute"    => partition_size, 
            "Max. No. of Tweets per Minute" => max_tweets_per_minute,
            "No. of Request per Window"     => window_size,
            "Max. No. of Tweets per Window" => max_tweets_per_window, 
            "No. of Windows per Hour"       => windows_per_hour,
            "Max. of Tweets per Hour"       => max_tweets_per_hour
        }
    end

    def terms
        @config["terms"]
    end

    def filters
        @config["filters"]
    end

    def terms_size 
        terms.length
    end 

    def filters_size
        filters.length
    end

    def line_size
        terms.length * filters.length
    end

    def window_size
        @config["rate_limits"]["window"]["size"]
    end

    def window_time
        @config["rate_limits"]["window"]["time"]
    end

    def partition_size
        b = @config["rate_limits"]["partition"]["time"]
        ((@config["rate_limits"]["window"]["size"] / @config["rate_limits"]["window"]["time"]) * b).ceil
    end

    def partition_time
        @config["rate_limits"]["partition"]["time"]
    end

    def partitions_to_complete 
        (line_size * 1.0 / partition_size).ceil
    end

    def partitions_per_window
        (window_size * 1.0 / partition_size).floor
    end

    def windows_to_complete
        (partitions_to_complete * 1.0 / partitions_per_window).ceil
    end

    def total_collection_time
        windows_to_complete * window_time
    end

    def total_completion_time
        remainder = partitions_to_complete % 6
        (windows_to_complete - 1) * window_time + remainder
    end

    def max_tweet_request
        @config["rate_limits"]["max_tweet_per_request"]
    end

    def max_tweets_per_minute
        partition_size * max_tweet_request
    end

    def max_tweets_per_window
        window_size * max_tweet_request
    end

    def windows_per_hour
        60 / window_time
    end

    def max_tweets_per_hour
        windows_per_hour * max_tweets_per_window
    end

    def item_idxs(item_idx)
        terms_idx   = item_idx / filters.length
        filters_idx = item_idx % filters.length 
        [terms_idx, filters_idx]
    end

    def item(item_idx)
        idxs = item_idxs(item_idx)
        [terms[idxs[0]], filters[idxs[1]]]
    end

    def get_partition_idxs(partition_idx)
        [partition_idx * partition_size, 
         partition_idx * partition_size + partition_size - 1]
    end

    def get_partition(partition_idx)
        items = []
        idxs = get_partition_idxs(partition_idx)
        (idxs[0]..idxs[1]).each do |idx| 
            items.push(item(idx))
        end
        items
    end

    def get_window_partition(window, idx)
        base_idx = partitions_per_window * window
        base_idx += idx
        get_partition(idx)
    end

    def get_item_partition(idx)
        idx / partition_size
    end

    def partition_mapping
        mapping = {}
        i = 0
        terms.each do |term| 
            mapping[term] = {}
            filters.each do |filter|
                mapping[term][filter] = 
                    get_item_partition(i)
                i += 1
            end
        end
        mapping
    end

    def blank_collection_table
        mapping = {}
        i = 0
        terms.each do |term| 
            mapping[term] = {}
            filters.each do |filter|
                mapping[term][filter] = 
                    0
                i += 1
            end
        end
        mapping
    end

    def get_remaining_time(partitions_done, time)
        remaining_time = total_completion_time.to_i * 60 
        remaining_time -= (partitions_done.to_i * (partition_time * 60).to_i)
        remaining_time -= time
        remaining_time
    end

    def remove_empty(items)
        clean_items = []
        items.each do |item| 
            if item[0] != "" || item[0] != nil then 
                clean_items.push(item)
            end
        end
        clean_items
    end
end