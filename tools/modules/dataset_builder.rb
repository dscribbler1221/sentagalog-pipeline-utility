require 'csv'
require_relative "../../preload"
require_relative "../helpers/allocator"

class DatasetBuilder 
   
    # meta variables 
    @project 
    @report 
    @config 
    @database

    # ------------- CONSTRUCTOR ------------------ #
    # initializes the builder and its components
    def initialize(config, project)

        puts "Initializing dataset buidler..." 
        # initialize meta variables 
        @report = create_report("reports/dataset_builder.md")
        @config = project.get_config('dataset_builder')
        @project = project

        # display collector set-up status
        @report.write "# Dataset Builder Set-up \n"
        project.report_runtime_info(@report)
        project.report_config("dataset_builder", @report)
        puts "Successfully initialized dataset buidler..." 

        # load database 
        @report.write "## Database Initialization \n"
        
        @report.write "### Database Configuration"
        @report.code JSON.pretty_generate @project.get_config("database")

        @database = @project.database

        @report.write "### Database Object"
        @report.code @project.database.inspect

    end

    # -------------------------- AGGREGATION METHODS ----------------------------#


    # aggregates the tweet from the dropzone folder
    def aggregation_info(source=@config["aggregator"]["default_source"]) 

        report = create_report("reports/dataset_builder.aggregation-info.md")
        puts "Aggregating tweets. Please wait. "

        report.write "# Dataset Builder : Aggregation Report"

        # get directories
        source_config   = @project.get_config("collector")["driver"][source]
        tweets_folder   = source_config + "tweets"
        meta_folder     = source_config + "meta"

        report.write "## Dropzone Configuration"
        report.kv_table({
            "Tweets Folder" => tweets_folder,
            "Meta Folder" => meta_folder
        })

        # get files from directory 
        tweets_files = ls_files(tweets_folder)
        meta_files = ls_files(meta_folder)

        # compute files in directory
        no_of_tweet_files = tweets_files.length
        no_of_meta_files = meta_files.length 
        
        report.write "---"

        report.write "## General Inventory"
        report.kv_table({
            "Tweet" => no_of_tweet_files,
            "Meta" => no_of_meta_files 
        }, ["Folder", "Files"])
        

        # identify search filters and search terms
        search_filters = {}
        search_terms = {}

        # count tweets in files
        i = 0
        meta_files.each do |file|
            puts "Processing file (#{i+1}/#{no_of_meta_files})" 
            meta = read_json(meta_folder + "/" + file)
            
            search_term = meta["search_term"] 
            search_filter = meta["search_filter"]
            count = meta["count"]

            # add search filters 
            if !search_filters[search_filter] then
                search_filters[search_filter] = {
                    "count" => count
                }
            else 
                search_filters[search_filter]["count"] += count
            end

            # add search terms
            if !search_terms[search_term] then
                search_terms[search_term] = {
                    "count" => count
                }
            else
                search_terms[search_term]["count"] += count
            end

            i += 1
        end


        # aggregate variables
        stats = {
            "search_terms" => get_stat_info("count", search_terms),
            "search_filters" => get_stat_info("count", search_filters)
        }


        report.write "### Search Terms Overview"
        report.kv_table(stats["search_terms"], ["Stats", "Value"])

        report.write "### Search Filters Overview"
        report.kv_table(stats["search_filters"], ["Stats", "Value"])
        report.write "---"


        report.write "## Detailed Inventory"

        # get other variables
        derive_vars("count", stats["search_terms"], search_terms, 3)
        derive_vars("count", stats["search_filters"], search_filters, 3)

        # get other variables
        
        report.write "### Search Terms"
        report.rc_table(search_terms)


        report.write "### Search Filters"
        report.rc_table(search_filters)


        puts "Successful. "
        puts "Please view detailed report at `reports/dataset_builder.aggregation-info.md`"

        {
            :source => source,
            :meta_files => meta_files,
            :tweets_files => tweets_files,
            :stats => stats,
            :search_terms => search_terms, 
            :search_filters => search_filters
        }
    end

    def aggregate(source=@config["aggregator"]["default_source"]) 
        puts "Recomputing aggregation info to be sure."
        payload = aggregation_info(source)

        puts "==================================================="
        puts "WARNING"
        puts "==================================================="
        puts "To be sure, please view the report file first. \n" + 
             "The next operation will perform " +
             "a transaction to the database that may be hard to undo."
        puts "Please view the report file at `reports/dataset_builder.aggregation-info.md` if valid."
        puts "------------"
        puts "Please type `proceed` if the information show in the report file is valid..."
        validate_input("> ", ["proceed"]) { |input, valid|
            if valid then 
                puts "Proceeding..."
                aggregate_commit(payload)
                break 
            else 
                puts "Invalid command, going back to main menu..."
                return
            end
        }
    end

    def aggregate_commit(payload)
        no_of_tweets = payload[:stats]["search_terms"]["total"]
        tweets_files = payload[:tweets_files]
        meta_files = payload[:meta_files]
        search_terms = payload[:search_terms]
        search_filters = payload[:search_filters]


        puts "Aggregating #{no_of_tweets} tweets #{tweets_files.length} files ..."
        puts "Please wait...." 

        # get aggregator configuration
        config = @config["aggregator"]
        meta_aggregator = config["meta"]
        tweet_aggregator = config["tweet"]
        
        # get folders 
        source = payload[:source]
        source_config = @project.get_config("collector")["driver"][source]
        meta_folder = source_config + "meta/"
        tweets_folder = source_config + "tweets/"

        @database.transaction do |db|
            # loop through the meta files and add the information
            # to the database
            i = 0
            meta_files.each do |meta_file| 
                puts "Forwarding meta file information to database. (#{i + 1}/#{meta_files.length} files)"
                meta = read_json(meta_folder + meta_file)
                bindings = {}
                meta_aggregator.each do |column, access_string|
                    bindings[column] = access_string_get(meta, access_string)
                end
                
                # get binding values
                binding_values = bindings.values

                # insert to database
                @database.add_meta(binding_values, db)

                i += 1
            end


            # loop through each topic and add the information to the dabase
            # to the database
            i = 0
            search_terms.keys.each do |term| 
                search_filters.keys.each do |filter|
                    tweet_file = term + " " + filter + ".json"
                    tweets = read_json(tweets_folder + tweet_file)["statuses"]
                    bindings = {}

                    puts "Forwarding tweet file to database. " + 
                        "(#{i + 1}/#{tweets_files.length} files) " + 
                        " - #{tweets.length} tweets "
                    # add each tweet to database
                    tweets.each do |tweet|
                      
                        # tweets
                        tweet_aggregator.each do |column, access_string|
                            bindings[column] = access_string_get(tweet, access_string)
                        end

                        tweet_text = tweet["full_text"]
                        emojis = @project.get_emojis_in_text(tweet_text).join("")

                        bindings["search_term"] = term
                        bindings["search_filter"] = filter
                        bindings["emojis"] = emojis
                        
                        # get binding values
                        binding_values = bindings.values

                        # puts bindings

                        @database.add_tweet(binding_values, db)
                    end 

                    i += 1
                end
            end
        end

        puts "Aggregation done."
    end


    # -------------------------- BUILDING METHODS ----------------------------#
    def build_info(dataset_name) 
        report = create_report("reports/dataset_builder.build-info.md")

        report.write "# Raw/Unclean Dataset Builder : " + 
                     "#{dataset_name.capitalize} Dataset Build Info"

        puts "Building #{dataset_name} dataset"

        # get build mode config
        config = @config["builder"][dataset_name]
        report.write "## Dataset Configuration"
        report.code JSON.pretty_generate config

        sql = config["query"]
        headers = config["headers"]

        unfiltered_words = 0
        unique_unfiltered_words = 0

        raw_stats = {
            "terms" => {}, 
            "filters" => {}
        }

        i = 0
        @database.query(sql) do |row|
            # write row to csv file 
            term = row[idx(headers, "term")]
            filter = row[idx(headers, "filter")]
            author = row[idx(headers, "author")]
            
            # terms
            if !raw_stats["terms"][term] then 
                raw_stats["terms"][term] = {
                    "count" => 1
                }
            else
                raw_stats["terms"][term]["count"] += 1
            end

            # filters
            if !raw_stats["filters"][filter] then
                raw_stats["filters"][filter] = {
                    "count" => 1
                }
            else
                raw_stats["filters"][filter]["count"] += 1
            end

            i += 1
        end

        stats = {
            "terms" => get_stat_info("count", raw_stats["terms"]),
            "filters" => get_stat_info("count", raw_stats["filters"])
        }

        summary = {
            "Total Tweets" => i
        }

        derive_vars("count", stats["terms"], raw_stats["terms"], 3)
        derive_vars("count", stats["filters"], raw_stats["filters"], 3)


        report.write "## General Summary"
        report.write "### Search Keywords"
        report.kv_table stats["terms"]

        report.write "### Search Filters "
        report.kv_table stats["filters"]

        report.write "## Detailed Stats"
        report.write "## Search Keywords "
        report.rc_table raw_stats["terms"]

        report.write "## Search Filters"
        report.rc_table raw_stats["filters"]


        puts "Please view detailed report at " + 
             "`reports/dataset_builder.build-info.md`"
    end 

    def build(dataset_name)
        puts "Building dataset #{dataset_name}"
        puts "Calling build info method..."
        build_info(dataset_name)

        config = @config["builder"][dataset_name]
        output = config["output"]

        CSV.open(output, "w:UTF-8") do |csv|
            csv << config["headers"]
            sql = config["query"]

            i = 0
            @database.query(sql) do |row|
                # write row to csv file 
                csv << row
                i += 1
            end
        end
    end


end