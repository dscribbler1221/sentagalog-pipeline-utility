require_relative "../../preload"
require_relative "../helpers/allocator"

class Collector
    
    # meta variables
    @project
    @report
    @progress_report
    @config

    # class variables
    @search_terms
    @search_filters
    @tokens
    @collector_options
    @search_options
    @driver

    # state 
    @current_partition

    # objects
    @allocator
    @time
    
    # ----------------- CONSTRUCTOR ---------------------- #
    def initialize(config, project)
        
        puts "Initializing collector..."
        @report = create_report("reports/collector.md")
        @config = project.get_config("collector")
        @project = project

    
        @current_partition = 0
        @time = 0

        # display collector set-up status
        @report.write "# Collector Set-up Report \n"
        project.report_runtime_info(@report)
        project.report_config("collector", @report)
        
        # load search terms
        load_search_terms()
        @report.write "## Search Terms \n"
        @report.write "### #{@search_terms.length} terms from `#{@config["search_terms"]}`: \n"
        @report.number_list @search_terms

        # load search filters
        load_search_filters()
        @report.write "## Search Filters \n"
        @report.write "### #{@search_filters.length} from `#{@config["search_filters"]}`: \n"
        @report.tabulate @search_filters

        # get other items 
        @tokens = @config["tokens"]
        @collector_options = @config["collector_options"]
        @search_options = @config["search_options"]
        @driver = @config["driver"]

        # initialize allocator
        load_allocator()
        
        @report.write "## Allocator Configuration \n"
        @report.kv_table @allocator.configuration

        @report.write "## Collection Time Information \n"
        @report.kv_table @allocator.collection_time_information

        @report.write "## Collection Amount Information \n"
        @report.kv_table @allocator.collection_amount_information

        @report.write "## Random Partition \n"
        @report.number_list random_partition

        @report.write "## Partition Mapping \n"
        puts @allocator.partition_mapping
        @report.rc_table @allocator.partition_mapping


        # create an empty payload for limit checks
        create_payload([])
        @report.write "## Initial Collector Payload  \n"
        @report.code JSON.pretty_generate payload

        puts "Successfully initialized collector..."


        
    end

    # ----------------- HELPER MODULES FUNCTION -----------------#
    # initializes the allocator
    def load_allocator()
        allocator_config = {
            "terms" => @search_terms, 
            "filters" =>  @search_filters,
            "rate_limits" => @config["rate_limits"]
        }
        @allocator = Allocator.new allocator_config
    end


    # -------------- PROGRESS REPORT WATCHER --------------------#
    def report_meta()
        @progress_report = create_report("reports/collector.progress.md")
     

        @progress_report.watch do
            time = @time
            report = @progress_report    
            
            remaining_time = @allocator.get_remaining_time(partitions_done, time) 

            # ---- report meta information about collection progress
            report.write "# Collection : Collection Progress"
            report.write "**Time Elapsed (Observation)**: " + time.to_s + 
                         " seconds" +  "&nbsp;" * 10, ""
            report.write """**Partitions Done**: 
                            #{partitions_done} /
                            #{@allocator.partitions_to_complete}""" + "&nbsp;" * 10, ""
            report.write """**Current Partition**: 
                            #{@current_partition}"
            report.write

            report.write """**Query Items Done:**: 
                            #{query_items_done} /
                            #{@allocator.line_size}""" + "&nbsp;" * 10, ""

            report.write """**Remaining Time:**: 
                            #{remaining_time / (60 * 60)} hours
                            #{remaining_time / 60 % 60} minutes
                            #{remaining_time % 60} seconds"""         
            report.write 

            @progress_report.write "**Total:** : #{running_total_tweets}" 

            report.write

            # ---- report progress table
            report.rc_table_hooked(meta_state) do
                |row_header, row, item| 

                # print header
                report.write "|" + row_header + "|", ""

                # print values
                row.each do |filter, value|
                    item = row_header + " " + filter  
                    # print green if metafile exists
                    if metafile_exists(item) then 
                        report.write "<span style='color:rgb(0, 159, 237)'>#{value}</span>|", ""
                    # else just print white
                    else 
                        report.write "#{value}|", ""
                    end
                end

                # print newline
                report.write
                
            end

            report.write


            # ---- report information about runtime configureation

            # print statistics
            @progress_report.kv_table(running_tweets, ["Topic", "No. of Tweets"]) 

            # display collector set-up status
            @report.write "# Collector Set-up Report \n"
            @project.report_runtime_info(report)
            @project.report_config("collector", report)
        end
    end

    # -------------- META STATE GETTER--------------------#
    # returns a hash containing the counts of tweet per item
    def meta_state(folder="dropzone")
        # loop through all partitions and get the current metas 
        collection_table = @allocator.blank_collection_table
            
        # loop through each search term in the collection table 
        collection_table.each do |term, filters| 
            # loop through each filter and get the meta configurations 
            # of that file from the dropzone folder
            filters.each do |filter, value| 
                item = term.to_s + " " + filter.to_s
                if metafile_exists(item, folder) then
                    meta = read_json(meta_file(item))
                    collection_table[term][filter] = meta["count"]
                end
            end
        end
        collection_table
    end

    # -------------- QUERY ITEM DONE COUNTER ---------------#
    # count how many query items has been done based on the meta files 
    def query_items_done
        done = 0
        @allocator.terms.each do |term| 
            @allocator.filters.each do |filter|
                item = term + " " + filter
                # check if file exists
                if metafile_exists(item) then 
                    done += 1
                end
            end
        end 
        done
    end 

    # -------------- PARTITIONS DONE COUNTER ---------------#
    # count how many query items has been done based on the meta files 
    def partitions_done
        query_items_done / @allocator.partition_size
    end 

    # -------------- RUNNING TWEET COUNTERS ---------------#
    # get the meta state
    def running_tweets
       terms = {}
       meta_state.each do |term, emojis| 
        terms[term] = emojis.values.sum
       end
       terms
    end

    # get total tweets 
    def running_total_tweets 
        running_tweets.values.sum
    end


    # -------------- METAFILE RELATED METHODS ---------------#

    def metafile_exists(item, folder="dropzone")
        File.exists? meta_file(item, folder)
    end

    def meta_file(item, folder="dropzone")
        @driver["dropzone"] + "meta/" + item.to_s + ".meta.json"
    end


    # -------------- METAFILE RELATED METHODS ---------------#
    # gets a sample partition
    def random_partition
        max = @allocator.partitions_to_complete - 1
        srand(Time.now.to_i)
        @allocator.get_partition((rand() * max).floor)
                  .collect { |v| v.join(" ") }
    end


    # -------------- SEARCH TERM AND FILTER GETTERS ---------------#

    # loads the search terms stated in the file config.yaml
    def load_search_terms()
        search_terms_file = @config["search_terms"]
        search_terms = read_file(search_terms_file)
        search_terms = search_terms.split("\n")
        search_terms = search_terms.collect { |v| v.strip }
        @search_terms = search_terms
    end

    # loads the search filters stated in the file config.yaml
    def load_search_filters() 
        search_filters_file = @config["search_filters"]
        search_filters = read_file(search_filters_file)
        search_filters = search_filters.split("\n")
        search_filters = search_filters.collect { |v| v.strip }
        @search_filters = search_filters
    end


    # -------------- SEQUENCE COLLECTION METHOD ---------------#
    
    # start collection sequence from a partition
    def start_collection_sequence(partition)
        
        @time = 0
        @current_partition = partition
        puts "Starting collection sequence. You can view a more detailed overview at" + 
             " collector.report.md"
        report_meta 

        # run once every second
        interval = set_interval_sync(1) {

            if @current_partition > @allocator.partitions_to_complete then 
                puts "Done!"
                break
            end

            # this is sure to run at 15 / parition_time times every window
            # significantly less than the 180 limit per window
            # on rate limit checks
            lapse = (60 * @config["rate_limits"]["partition"]["time"]).ceil
            if @time % lapse.to_i == 0 then 
                
                puts "Attempting to collect partition #{@current_partition}"
                puts "Checking limit..."
                # if the remaining limit is greather than the partition size
                # that's the time that we will only collect
                limit = collect_limit["remaining"] 
                over = rate_limit_status["remaining"]
                puts "Limit: " + limit.to_s + "/" + over.to_s
                if limit > @allocator.partition_size then
                    puts "Ready to collect."
                    puts "Collecting."
                    
                    c = collect_partition(@current_partition)

                    puts "Successfully collected partition #{@current_partition}."
                    puts "Collecting."
                    puts "----"

                    @current_partition += 1
                else
                    puts "Not read to collect. Skipping."
                end

            else 
                puts "Time until next round: " + (lapse - @time % lapse).to_s + " seconds" 
            end
            
          
            @time += 1
        }
    end

    # -------------- PARTITION COLLECTION METHOD ---------------#

    # collects a specific partition of a window
    def collect_partition(partition)

        write_json("resources/collect.failsafe.json")

        # get items
        items = @allocator.get_partition(partition)
        

        if partition > @allocator.partitions_to_complete then 
            puts "Out of bounds."
            items_c
        end

        # create payload file with items, tokens and collector options
        items_c = create_payload(items)

        # send request to collector
        collect()
        items_c
    end

    # -------------- PARTITION AGGREGRATION METHODS ---------------#
    
    # gets the tweets of a certain partition from the dropzone
    def get_tweets(window, partition)
        # get the set of items covered by the partition
        items = @allocator.get_window_partition(window, partition)
        items = items.collect { |v| v.join(" ") }

        # get the tweet files from the dropzone folder
        tweets = {}
        items.each do |file|
            tweet_file = @driver["dropzone"]["tweets"] + file.to_s + ".json"
            tweets[file] = read_json(tweet_file)
        end
        tweets
    end

    # gets the metas of a certain partition from the dropzone
    def get_metas(window, partition)
        # get the set of items covered by the partition
        items = @allocator.get_window_partition(window, partition)
        items = items.collect { |v| v.join(" ") }

        # get the tweet files from the dropzone folder
        metas = {}
        items.each do |file|
            meta_file = @driver["dropzone"]["meta"] + file.to_s + ".meta.json"
            metas[file] = read_json(meta_file)
        end
        metas
    end


    # -------------- PAYLOAD OPERATION METHODS ---------------#

    # creates a payload that will be read the collector and limit_checker
    def create_payload(items)
        items = @allocator.remove_empty(items)

        payload = {
            "search_items"      => items,
            "tokens"            => @tokens,
            "collector_options" => @collector_options,
            "search_options"    => @search_options        
        }

        # write payload to file
        payload_file = @driver["payload_file"]
        payload_file = write_json(payload_file, payload)
        payload_file.close

        items
    end

    # reads the payload data from the payload file created
    def payload 
        read_json(@driver["payload_file"])
    end


    # ------------------ COLLECTION HOOKS ------------------#

   
    # ------------------ DRIVER METHODS ------------------ #

    # function to check rate limit
    def check_limit()
        response = run_node(@driver["limit_checker"])
        @limit = read_json(@driver["limits_file"])
        @limit
    end

    # get rate limit 
    def rate_limit(resource, endpoint) 
        check_limit["resources"][resource][endpoint]
    end

    # get current collect limit
    def collect_limit()
        rate_limit("search", "/search/tweets")   
    end

    # get current check limit
    def rate_limit_status()
        rate_limit("application", "/application/rate_limit_status")   
    end

    # functions to send requests and receive responses from collector.js
    # dropzone folder when the request has been done
    def collect()
        puts run_node(@driver["collector"])
    end


    # ------------------ CUSTOM GETTERS ------------------ #
    def allocator 
        @allocator
    end

    def search_terms 
        @search_terms
    end

    def search_filters 
        @search_filters 
    end

  
end 