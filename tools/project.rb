require_relative '../preload'
require_relative 'modules/collector'
require_relative 'modules/dataset_builder'

class Project
    
    # configuration objects
    @global_config
    @config

    # report object
    @report

    # class vars
    @runtime_name
    @resources
    @resource_meta

    @database

    # load modules
    @collector
    @dataset_builder

    # -------------- CONSTRUCTOR FUNCTION ------------------- #

    def initialize(config, runtime_name) 
        
        puts "Initializing project"

        # create report file
        @report = create_report("reports/project.md") 
        
        # store config files
        @global_config = config
        @runtime_name = runtime_name
        @config = @global_config["project"]
        

        # write project report file
        @report.write "# Project Report"
        report_runtime_info(@report)
        report_config("project", @report)

        # load database
        load_database()
 
        # load resources
        load_resources() 
        report_resource_info()

        # load modules
        load_collector()
        load_builder()

        puts "Successfully initialized project"

    end


    # -------------- DATABASE LOADER METHOD --------------- #
    
    def load_database()
        db_config = get_config("database")
        @database = SentagalogDatabase.new db_config, self
    end

    # -------------- RESOURCE LOADER METHOD -------------#
    def load_resources()
        @resources = {}
        @report.write "## Resource Loading"

        # load resources from files
        @config["resources"].each do |resource_type, subresources| 
            @report.write "### Loading resource set : `#{resource_type}`"  
            @resources[resource_type] = {}
            subresources.each do |resource_name, resource_file|
                resource = load_resource(resource_type, resource_name)

                @report.write """> Loading resource file 
                    `#{resource_type}/#{resource_name}`
                    from `#{resource_file}`
                    (#{resource.split("\n").length} lines) \n"""

                @resources[resource_type][resource_name] = resource
            end
        end
    end


    def load_resource(resource_type, resource_name, type="text")
        if type == "text" then
            resource_file = 
                @config["resources"][resource_type][resource_name] 
            return read_file(resource_file)
        else 
            puts "Invalid resource loading parameters."
        end
    end


    # -------------- MODULE LOADING METHOD-------------#

    def load_collector()
        collector_config = get_config("collector")
        @collector = Collector.new collector_config, self
    end

    def load_builder()
        builder_config = get_config("dataset_builder")
        @dataset_builder = DatasetBuilder.new builder_config, self
    end

    # -------------- MODULE LOADING METHOD-------------#
    def report_resource_info() 
        @resource_meta = {}
        @report.write "## No. of Items per Resource \n"

        # count the number of items in each file
        # splitting by n is used since the files'
        # values are line separated
        @resources.each do |resource_type, subresources|
            @resource_meta[resource_type] = {} 
            subresources.each do |resource_name, resource_file|
                resource = @resources[resource_type][resource_name]
                @resource_meta[resource_type][resource_name] = 
                    resource.split("\n").length
            end
        end 

        # resources metadata object
        @report.code JSON.pretty_generate @resource_meta
    end



 
    # -------------- REPORT UTILITIES -------------#
    def report_runtime_info(report)
        report.write "## Runtime Information"
        report.kv_table(@runtime_name, ["INFO", "VALUE"])
    end

    def report_global_config(report) 
        report.write "## Global Configuration" 
        report.code JSON.pretty_generate @global_config
    end 

    def report_config(modul, report)
        report.write "## Module Configuration" 
        report.code JSON.pretty_generate @global_config[modul]
    end


    # -------------- CUSTOM UTILITY FUNCTIONS ------------#

    def global_config 
        @global_config
    end

    def get_config(subconfig)
        @global_config[subconfig]
    end

    def get_runtime_info
        @runtime_name
    end 

    def get_emojis_in_text(text)
        emojis_list = @collector.search_filters
        emojis = []
        (0..text.length - 1).each do |i| 
            letter = text[i]
            (0..emojis_list.length - 1).each do |j| 
                emoji = emojis_list[j]
                if (emoji == letter) && !(emojis.include? letter) then 
                    emojis.push(letter)
                end
            end 
        end
        emojis
    end

    # ----- export components ----- #

    def database 
        @database
    end 
    
    def collector
        @collector
    end

    def dataset_builder
        @dataset_builder
    end
end

